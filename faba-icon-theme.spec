%global		debug_package	%{nil}

Name:		faba-icon-theme
Version:	4.1.2
Release:	1%{?dist}
Summary:	Modern, elegant icon theme with Tango influences

License:	LGPLv3+ or CC-BY-SA
URL:		https://snwh.org/moka
Source0:	https://github.com/snwh/faba-icon-theme/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:	autoconf
BuildRequires:	automake

BuildArch:	noarch


%description
%{summary}.


%prep
%autosetup

%build
NOCONFIGURE=1 sh autogen.sh
%configure
%make_build
chmod a-x README.md


%install
%make_install


%files
%license COPYING
%doc README.md AUTHORS
%{_datadir}/icons/Faba


%changelog
* Sat Nov 26 2016 Link Dupont <linkdupont@fedoraproject.org> - 4.1.2-1
- Initial package
